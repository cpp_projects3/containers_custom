#ifndef S21_CONTAINERS_SRC_S21_TREE_H_
#define S21_CONTAINERS_SRC_S21_TREE_H_

namespace s21 {

template <class T, typename Cmp = std::less<T>>
class AVLTree {
 private:
  struct AVLTreeNode;
  struct AVLTreeIterator;
  struct AVLTreeIteratorConst;

 public:
  // types and aliases
  using value_type = T;
  using reference = T &;
  using pointer = T *;
  using const_pointer = const T *;
  using const_reference = const T &;
  using iterator = AVLTreeIterator;
  using const_iterator = AVLTreeIteratorConst;
  using size_type = std::size_t;

  // constructors and assignment ops
  AVLTree() : root_(nullptr), size_(0) {}

  AVLTree(const AVLTree &other) : AVLTree() {
    AVLTreeNode *other_root = CopyTree(other.root_);
    root_ = other_root;
    SwapRecursive(root_);
    size_ = other.size_;
  }

  AVLTree(AVLTree &&other) noexcept : AVLTree() { SwapTree(other); }

  AVLTree &operator=(const AVLTree &other) {
    AVLTreeNode *other_root = CopyTree(other.root_);
    DeleteRecursive(root_);
    root_ = other_root;
    SwapRecursive(root_);
    size_ = other.size_;
    return *this;
  }

  AVLTree &operator=(AVLTree &&other) noexcept {
    DeleteRecursive(root_);
    root_ = nullptr;
    SwapTree(other);
    return *this;
  }

  // destructor
  ~AVLTree() {
    DeleteRecursive(root_);
    root_ = nullptr;
  };

  // methods
  void SwapTree(AVLTree &other) noexcept {
    std::swap(root_, other.root_);
    std::swap(size_, other.size_);
    SwapRecursive(root_);
  }

  std::pair<iterator, bool> Insert(const_reference data) {
    auto it_tmp = Find(data);
    bool scnd = (root_ == nullptr) ? true : it_tmp == End();
    if (!scnd) {
      return std::make_pair(it_tmp, scnd);
    } else {
      iterator it = iterator();
      InsertNode(data, root_, nullptr);
      it.node_ptr = Find(data).node_ptr;
      it.root = it.node_ptr->root;
      auto pair = std::make_pair(it, scnd);
      return pair;
    }
  }

  void DeleteNode(const_reference data) { DeleteNode(data, root_); }

  size_type MaxSize() {
    return (std::numeric_limits<value_type>::max() * (sizeof(AVLTreeNode) + 1) /
                2 +
            sizeof(AVLTree));
  }

  size_type Size() { return size_; }

  bool Empty() const noexcept { return size_ == 0; }

  iterator Begin() {
    iterator it;
    struct AVLTreeNode *tmp_node = root_;
    if (tmp_node != nullptr) {
      while (tmp_node->left) {
        tmp_node = tmp_node->left;
      }
    } else {
      tmp_node = nullptr;
    }

    it.node_ptr = tmp_node;
    it.root = tmp_node ? tmp_node->root : nullptr;
    return it;
  }

  iterator End() {
    auto it = iterator();
    it.root = root_;
    return it;
  }

  iterator Find(const_reference key) {
    AVLTreeNode *node_ptr = Find(key, root_);
    if (node_ptr != nullptr) {
      return iterator(node_ptr);
    } else {
      return End();
    }
  }

  void Merge(AVLTree &other) {
    auto it = other.Begin();
    std::vector<value_type> items;
    while (it != other.End()) {
      if (Insert(*it).second) {
        items.push_back(*it++);
      } else {
        ++it;
      }
    }
    for (value_type item : items) {
      other.DeleteNode(item);
    }
  }

  AVLTreeNode *GetRoot() { return root_; }

  void PrintMap() { PrintMap("", root_, false); }

  void PrintMap(const std::string &prefix, const AVLTreeNode *node_ptr,
                bool is_left) {
    if (node_ptr != nullptr) {
      std::cout << prefix;
      std::cout << (is_left ? "├──l──" : "└──r──");

      if (node_ptr->parent) {
        std::cout << node_ptr->data.first << ":" << node_ptr->data.second
                  << " h: " << node_ptr->height
                  << " p: " << node_ptr->parent->data.first << ":"
                  << node_ptr->parent->data.second << " rt:" << node_ptr->root
                  << std::endl;
      } else {
        std::cout << node_ptr->data.first << ":" << node_ptr->data.second
                  << " h: " << node_ptr->height << " p: n rt:" << node_ptr->root
                  << std::endl;
      }

      // std::cout << node_ptr->data << std::endl;

      PrintMap(prefix + (is_left ? "│     " : "      "), node_ptr->left, true);
      PrintMap(prefix + (is_left ? "│     " : "      "), node_ptr->right,
               false);
    }
  }

 private:
  // derived structs
  struct AVLTreeNode {
    AVLTreeNode()
        : root(this),
          parent(nullptr),
          left(nullptr),
          right(nullptr),
          data(value_type{}),
          height(1U) {}

    AVLTreeNode(const value_type &data_input, struct AVLTreeNode *Root)
        : root(Root ? Root : this),
          parent(nullptr),
          left(nullptr),
          right(nullptr),
          data(data_input),
          height(1U) {}

    AVLTreeNode(value_type &&data_input)
        : root(this),
          parent(nullptr),
          left(nullptr),
          right(nullptr),
          data(std::move(data_input)),
          height(1U) {}

    void UpdateHeight() {
      height = 1 + std::max(left ? left->height : 0, right ? right->height : 0);
    }

    int GetBalanceFactor() {
      return (left ? left->height : 0) - (right ? right->height : 0);
    }

    struct AVLTreeNode *GetNextNode() const noexcept {
      struct AVLTreeNode *p;
      struct AVLTreeNode *tmp = const_cast<AVLTreeNode *>(this);

      if (tmp->right != nullptr) {
        tmp = tmp->right;
        while (tmp->left != nullptr) {
          tmp = tmp->left;
        }
      } else {
        p = tmp->parent;
        while (p != nullptr && tmp == p->right) {
          tmp = p;
          p = p->parent;
        }
        if (p == nullptr && tmp == tmp->root) {
          tmp = nullptr;
        } else {
          tmp = p;
        }
      }

      return tmp;
    }

    struct AVLTreeNode *GetPrevNode() const noexcept {
      AVLTreeNode *p;
      struct AVLTreeNode *tmp = const_cast<AVLTreeNode *>(this);

      if (tmp->left != nullptr) {
        tmp = tmp->left;
        while (tmp->right != nullptr) {
          tmp = tmp->right;
        }
      } else {
        p = tmp->parent;
        while (p != nullptr && tmp == p->left) {
          tmp = p;
          p = p->parent;
        }
        if (p == nullptr && tmp == tmp->root) {
          tmp = nullptr;
        } else {
          tmp = p;
        }
      }

      return tmp;
    }

    struct AVLTreeNode *root;
    struct AVLTreeNode *parent;
    struct AVLTreeNode *left;
    struct AVLTreeNode *right;

    value_type data;
    size_t height;
  };

  struct AVLTreeIterator {
    AVLTreeIterator() : node_ptr(nullptr), root(nullptr) {}

    AVLTreeIterator(AVLTreeNode *p)
        : node_ptr(p), root(p ? p->root : nullptr) {}

    AVLTreeIterator(const AVLTreeIterator &it)
        : node_ptr(it.node_ptr), root(it.root) {}

    AVLTreeIterator &operator=(const AVLTreeIterator &it) {
      node_ptr = it.node_ptr;
      root = it.root;
      return *this;
    }

    bool operator==(const AVLTreeIterator &it) const {
      return node_ptr == it.node_ptr;
    }

    bool operator!=(const AVLTreeIterator &it) const {
      return node_ptr != it.node_ptr;
    }

    // preincrement
    AVLTreeIterator &operator++() {
      if (node_ptr) {
        node_ptr = node_ptr->GetNextNode();
        root = node_ptr ? node_ptr->root : nullptr;
      }
      return *this;
    }

    // postincrement
    AVLTreeIterator operator++(int) {
      AVLTreeIterator old(*this);
      ++(*this);
      return old;
    }

    // predecrement
    AVLTreeIterator &operator--() {
      if (node_ptr) {
        node_ptr = node_ptr->GetPrevNode();
        root = node_ptr ? node_ptr->root : nullptr;
      } else if (root) {
        auto tmp = root;
        while (tmp->right != nullptr) {
          tmp = tmp->right;
        }
        node_ptr = tmp;
        root = node_ptr->root;
      }
      return *this;
    }

    // postdecrement
    AVLTreeIterator operator--(int) {
      AVLTreeIterator old(*this);
      --(*this);
      return old;
    }

    reference operator*() const {
      value_type null_val{};
      return (node_ptr) ? node_ptr->data : null_val;
    }

    pointer operator->() const { return &(node_ptr->data); }

    AVLTreeNode *node_ptr;
    AVLTreeNode *root;
  };

  struct AVLTreeIteratorConst {
    AVLTreeIteratorConst() : node_ptr(nullptr), root(nullptr) {}

    AVLTreeIteratorConst(AVLTreeNode *p)
        : node_ptr(p), root(p ? p->root : nullptr) {}

    AVLTreeIteratorConst(const AVLTreeIterator &it)
        : node_ptr(it.node_ptr), root(it.root) {}

    friend bool operator==(const AVLTreeIteratorConst &it,
                           const AVLTreeIteratorConst &it2) {
      return it2.node_ptr == it.node_ptr;
    }

    friend bool operator!=(const AVLTreeIteratorConst &it,
                           const AVLTreeIteratorConst &it2) {
      return it2.node_ptr != it.node_ptr;
    }

    // preincrement
    AVLTreeIteratorConst &operator++() {
      if (node_ptr) {
        node_ptr = node_ptr->GetNextNode();
        root = node_ptr ? node_ptr->root : nullptr;
      }
      return *this;
    }

    // postincrement
    AVLTreeIteratorConst operator++(int) {
      AVLTreeIteratorConst old(*this);
      ++(*this);
      return old;
    }

    // predecrement
    AVLTreeIteratorConst &operator--() {
      if (node_ptr) {
        node_ptr = node_ptr->GetPrevNode();
        root = node_ptr ? node_ptr->root : nullptr;
      } else if (root) {
        auto tmp = root;
        while (tmp->right != nullptr) {
          tmp = tmp->right;
        }
        node_ptr = tmp;
        root = node_ptr->root;
      }
      return *this;
    }

    // postdecrement
    AVLTreeIteratorConst operator--(int) {
      AVLTreeIteratorConst old(*this);
      --(*this);
      return old;
    }

    const_reference operator*() const {
      value_type null_val{};
      return (node_ptr) ? node_ptr->data : null_val;
    }

    const_pointer operator->() const { return &(node_ptr->data); }

    const AVLTreeNode *node_ptr;
    const AVLTreeNode *root;
  };

  // private methods
  void DeleteRecursive(struct AVLTreeNode *node) {
    if (node != nullptr) {
      if (node->left != nullptr) {
        DeleteRecursive(node->left);
      }
      if (node->right != nullptr) {
        DeleteRecursive(node->right);
      }
      delete node;
    }
  }

  /**
   * @brief changes root property to this.root_ for every node starting from
   * node_ptr
   */
  void SwapRecursive(struct AVLTreeNode *node_ptr) {
    if (node_ptr) {
      if (node_ptr->left != nullptr) {
        SwapRecursive(node_ptr->left);
      }
      if (node_ptr->right != nullptr) {
        SwapRecursive(node_ptr->right);
      }
      node_ptr->root = root_;
    }
  }

  void PrintTree() { PrintTree("", root_, false); }

  void PrintTree(const std::string &prefix, const AVLTreeNode *node_ptr,
                 bool is_left) {
    if (node_ptr != nullptr) {
      std::cout << prefix;
      std::cout << (is_left ? "├──l──" : "└──r──");
      if (node_ptr->parent) {
        std::cout << node_ptr->data << " h: " << node_ptr->height
                  << " p: " << node_ptr->parent->data
                  << " rt:" << node_ptr->root << std::endl;
      } else {
        std::cout << node_ptr->data << " h: " << node_ptr->height
                  << " p: n rt:" << node_ptr->root << std::endl;
      }

      // std::cout << node_ptr->data << std::endl;

      PrintTree(prefix + (is_left ? "│     " : "      "), node_ptr->left, true);
      PrintTree(prefix + (is_left ? "│     " : "      "), node_ptr->right,
                false);
    }
  }

  /*
      x      ->      y
     / \     ->     / \
    t1  y    ->    x   t3
       / \   ->   / \
     t2  t3  ->  t1  t2
  */
  struct AVLTreeNode *RotateLeft(struct AVLTreeNode *x) {
    if (x->parent == nullptr) {
      root_ = x->right;
      SwapRecursive(x);
    };

    struct AVLTreeNode *y = x->right;
    struct AVLTreeNode *t2 = y->left;

    y->left = x;
    y->parent = x->parent;
    x->parent = y;
    x->right = t2;
    if (t2) t2->parent = x;

    x->UpdateHeight();
    y->UpdateHeight();

    return y;
  };

  /*
      y      ->     x
     / \     ->    / \
    x   t4   ->   t2  y
   / \       ->      / \
  t2  t3     ->    t3   t4
  */
  struct AVLTreeNode *RotateRight(struct AVLTreeNode *y) {
    if (y->parent == nullptr) {
      root_ = y->left;
      SwapRecursive(y);
    }
    struct AVLTreeNode *x = y->left;
    struct AVLTreeNode *t3 = x->right;

    x->right = y;
    x->parent = y->parent;
    y->parent = x;
    y->left = t3;
    if (t3) t3->parent = y;

    y->UpdateHeight();
    x->UpdateHeight();

    return x;
  };

  void InsertNode(const_reference data) { InsertNode(data, root_, nullptr); }

  struct AVLTreeNode *InsertNode(const_reference data,
                                 struct AVLTreeNode *node_ptr,
                                 struct AVLTreeNode *parent_ptr) {
    if (root_ == nullptr) {
      root_ = new struct AVLTreeNode(data, nullptr);
      ++size_;
      return root_;
    }
    if (node_ptr == nullptr) {
      struct AVLTreeNode *new_node_ptr = new struct AVLTreeNode(data, root_);
      new_node_ptr->parent = parent_ptr;
      ++size_;
      return new_node_ptr;
    } else {
      if (cmp_(data, node_ptr->data)) {
        node_ptr->left = InsertNode(data, node_ptr->left, node_ptr);
      } else if (cmp_(node_ptr->data, data)) {
        node_ptr->right = InsertNode(data, node_ptr->right, node_ptr);
      } else {
        return node_ptr;
      }
    }  // IDEA for multiset: if data == node_ptr->data then store it in
       // additional field

    node_ptr->UpdateHeight();
    int balance = node_ptr->GetBalanceFactor();

    if (balance > 1 && cmp_(data, node_ptr->left->data))
      return RotateRight(node_ptr);

    if (balance < -1 && cmp_(node_ptr->right->data, data))
      return RotateLeft(node_ptr);

    if (balance > 1 && cmp_(node_ptr->left->data, data)) {
      node_ptr->left = RotateLeft(node_ptr->left);
      return RotateRight(node_ptr);
    }

    if (balance < -1 && cmp_(data, node_ptr->right->data)) {
      node_ptr->right = RotateRight(node_ptr->right);
      return RotateLeft(node_ptr);
    }

    return node_ptr;
  };

  void SwapNodes(struct AVLTreeNode *node_ptr1, struct AVLTreeNode *node_ptr2) {
    if (node_ptr1->left) node_ptr1->left->parent = node_ptr2;
    if (node_ptr1->right) node_ptr1->right->parent = node_ptr2;
    if (node_ptr2->left) node_ptr2->left->parent = node_ptr1;
    if (node_ptr2->right) node_ptr2->right->parent = node_ptr1;
    std::swap(node_ptr1->left, node_ptr2->left);
    std::swap(node_ptr1->right, node_ptr2->right);
    std::swap(node_ptr1->height, node_ptr2->height);

    if (node_ptr1->parent) {
      if (node_ptr1->parent->left == node_ptr1)
        node_ptr1->parent->left = node_ptr2;
      if (node_ptr1->parent->right == node_ptr1)
        node_ptr1->parent->right = node_ptr2;
    }
    if (node_ptr2->parent) {
      if (node_ptr2->parent->left == node_ptr2)
        node_ptr2->parent->left = node_ptr1;
      if (node_ptr2->parent->right == node_ptr2)
        node_ptr2->parent->right = node_ptr1;
    }
    std::swap(node_ptr1->parent, node_ptr2->parent);

    if (node_ptr1->parent == nullptr) {
      root_ = node_ptr1;
    }
    if (node_ptr2->parent == nullptr) {
      root_ = node_ptr2;
    }
    SwapRecursive(root_);
  }

  struct AVLTreeNode *DeleteNode(value_type data,
                                 struct AVLTreeNode *node_ptr) {
    if (node_ptr == nullptr) return node_ptr;
    if (cmp_(data, node_ptr->data)) {
      node_ptr->left = DeleteNode(data, node_ptr->left);
    } else if (cmp_(node_ptr->data, data)) {
      node_ptr->right = DeleteNode(data, node_ptr->right);
    } else {
      // if has less than two children
      if ((node_ptr->left == nullptr) || (node_ptr->right == nullptr)) {
        struct AVLTreeNode *temp =
            node_ptr->left ? node_ptr->left : node_ptr->right;

        // if has no children
        if (temp == nullptr) {
          temp = node_ptr;
          node_ptr = nullptr;
          if (root_ == temp) root_ = nullptr;

          // if has one child
        } else {
          SwapNodes(node_ptr, temp);
          std::swap(node_ptr, temp);
        }
        if (temp->parent) {
          if (temp == temp->parent->left) temp->parent->left = nullptr;
          if (temp == temp->parent->right) temp->parent->right = nullptr;
        } else {
        }
        delete temp;
        temp = nullptr;
        --size_;

        // if has both children
      } else {
        struct AVLTreeNode *temp = node_ptr->left;
        while (temp->right != nullptr) {
          temp = temp->right;
        }
        SwapNodes(node_ptr, temp);
        std::swap(node_ptr, temp);
        node_ptr->left = DeleteNode(temp->data, node_ptr->left);
      }
    }

    if (node_ptr == nullptr) return node_ptr;

    node_ptr->UpdateHeight();

    int balance = node_ptr->GetBalanceFactor();

    if (balance > 1 && node_ptr->left->GetBalanceFactor() >= 0) {
      return RotateRight(node_ptr);
    }

    if (balance < -1 && node_ptr->right->GetBalanceFactor() <= 0) {
      return RotateLeft(node_ptr);
    }

    if (balance > 1 && node_ptr->left->GetBalanceFactor() < 0) {
      node_ptr->left = RotateLeft(node_ptr->left);
      return RotateRight(node_ptr);
    }

    if (balance < -1 && node_ptr->right->GetBalanceFactor() > 0) {
      node_ptr->right = RotateRight(node_ptr->right);
      return RotateLeft(node_ptr);
    }

    return node_ptr;
  };

  AVLTreeNode *CopyTree(const AVLTreeNode *node) {
    if (node == nullptr) {
      return nullptr;
    } else {
      AVLTreeNode *copy = new AVLTreeNode(*node);
      copy->root = root_;
      if (node->left) {
        copy->left = CopyTree(node->left);
      }
      if (node->right) {
        copy->right = CopyTree(node->right);
      }
      return copy;
    }
  }

  struct AVLTreeNode *Find(const_reference key, struct AVLTreeNode *node_ptr) {
    if (node_ptr == nullptr) return nullptr;
    if (!cmp_(key, node_ptr->data) && !cmp_(node_ptr->data, key)) {  // ==
      return node_ptr;
    } else if (cmp_(node_ptr->data, key) && node_ptr->right != nullptr) {
      return Find(key, node_ptr->right);
    } else if (cmp_(key, node_ptr->data) && node_ptr->left != nullptr) {
      return Find(key, node_ptr->left);
    } else {
      return nullptr;
    }
  }

  // private data
  struct AVLTreeNode *root_;
  size_type size_;
  Cmp cmp_;
};
}  // namespace s21

#endif  // S21_CONTAINERS_SRC_S21_TREE_H_
