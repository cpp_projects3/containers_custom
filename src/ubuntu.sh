#!/bin/bash
docker build . -t ubuntu:1.0 -f ./Dockerfile &&
docker run -it -v $(pwd):/project --entrypoint="/bin/bash" --rm ubuntu:1.0
