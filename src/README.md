# Project - S21_Containers

## About
Custom library that implements the basic standard C++ container classes with a full set of standard methods and attributes for element handling, container capacity checking and iteration:
- list
- map
- queue
- set
- stack
- vector
- array*
- multiset*
---

## Build
```
cd src
make
```
## Test for leaks
1. run docker
2. from host terminal
```
cd src
bash ubuntu.sh
```
3. from container terminal
```
cd project
make grind
```
## Test
```
cd src
make test
```
## Format all files to Google style
```
cd src
make format
```