#ifndef S21_CONTAINERS_SRC_S21_MAP_H_
#define S21_CONTAINERS_SRC_S21_MAP_H_

#include "s21_tree.h"

namespace s21 {
template <class Key, class Type>
class map {
 public:
  using key_type = Key;
  using mapped_type = Type;
  using value_type = std::pair<const key_type, mapped_type>;
  using reference = value_type &;
  using const_reference = const value_type &;
  using size_type = std::size_t;

  struct Comparator {
    bool operator()(const_reference value1, const_reference value2) const {
      return value1.first < value2.first;
    }
  };

  using tree_type = AVLTree<value_type, Comparator>;
  using iterator = typename tree_type::iterator;
  using const_iterator = typename tree_type::const_iterator;

  void print() { tree_->PrintMap(); }

  map() : tree_(new tree_type{}) {}

  map(std::initializer_list<value_type> const &items) : map() {
    for (auto it = items.begin(); it != items.end(); it++) {
      insert(*it);
    }
  }

  map(const map &other) : tree_(new tree_type(*other.tree_)) {}

  map(map &&other) noexcept : tree_(new tree_type(std::move(*other.tree_))) {}

  map &operator=(map &&other) noexcept {
    *tree_ = std::move(*other.tree_);
    return *this;
  }

  ~map() {
    delete tree_;
    tree_ = nullptr;
  }

  Type &at(const key_type &key) {
    value_type stub(key, mapped_type{});
    auto it = tree_->Find(stub);
    if (it == end()) throw std::out_of_range("no such element");
    return (*it).second;
  }

  const Type &at(const key_type &key) const {
    return const_cast<map<Key, Type> *>(this)->at(key);
  }

  Type &operator[](const key_type &key) {
    value_type stub(key, mapped_type{});
    iterator it = tree_->Find(stub);

    if (it == end()) {
      std::pair<iterator, bool> result = tree_->Insert(stub);
      return (*result.first).second;
    } else {
      return (*it).second;
    }
  }

  iterator begin() noexcept { return tree_->Begin(); }

  const_iterator begin() const noexcept { return tree_->Begin(); }

  iterator end() noexcept { return tree_->End(); }

  const_iterator end() const noexcept { return tree_->End(); }

  bool empty() const noexcept { return tree_->Empty(); }

  size_type size() const noexcept { return tree_->Size(); }

  size_type max_size() const noexcept { return tree_->MaxSize(); }

  void clear() noexcept {
    delete tree_;
    tree_ = new tree_type{};
  }

  std::pair<iterator, bool> insert(const value_type &value) {
    return tree_->Insert(value);
  }

  std::pair<iterator, bool> insert(const key_type &key, const Type &obj) {
    return tree_->Insert(value_type{key, obj});
  }

  std::pair<iterator, bool> insert_or_assign(const key_type &key,
                                             const Type &obj) {
    iterator result = tree_->Find(value_type{key, obj});

    if (result == end()) {
      return tree_->Insert(value_type{key, obj});
    }
    (*result).second = obj;
    return {result, false};
  }

  void erase(iterator pos) noexcept { tree_->DeleteNode(*pos); }

  void swap(map &other) noexcept { tree_->SwapTree(*other.tree_); }

  void merge(map &other) noexcept { tree_->Merge(*other.tree_); }

  bool contains(const key_type &key) const noexcept {
    value_type stub(key, mapped_type{});
    return tree_->Find(stub) != end();
  }

 private:
  tree_type *tree_;
};
}  // namespace s21

#endif  // S21_CONTAINERS_SRC_S21_MAP_H_
