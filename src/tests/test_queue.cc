#include "main.h"

class QueueTest : public ::testing::Test {
 protected:
  void SetUp() override {}
  s21::queue<int> my_queue_empty_;
  s21::queue<int> my_queue_full_{2, 5, 7, 10};
  struct new_type {
    char a_char = 'd';
    double a_double = 2.3;
  };
  new_type buff;
};

TEST_F(QueueTest, default_constructor) {
  ASSERT_EQ(my_queue_empty_.size(), 0);
  ASSERT_TRUE(my_queue_empty_.empty());
}

TEST_F(QueueTest, list_constructor) {
  ASSERT_FALSE(my_queue_full_.empty());
  ASSERT_EQ(my_queue_full_.size(), 4);
  ASSERT_EQ(my_queue_full_.front(), 10);
}

TEST_F(QueueTest, copy_constructor) {
  s21::queue<int> new_stack(my_queue_full_);
  ASSERT_FALSE(new_stack.empty());
  ASSERT_EQ(new_stack.size(), 4);
  ASSERT_EQ(new_stack.front(), 10);
  new_stack.pop();
  ASSERT_EQ(new_stack.size(), 3);
  ASSERT_EQ(new_stack.back(), 5);
}

TEST_F(QueueTest, move_contructor) {
  s21::queue<int> new_stack(std::move(my_queue_full_));
  ASSERT_EQ(new_stack.front(), 10);
  ASSERT_EQ(new_stack.size(), 4);
  ASSERT_FALSE(new_stack.empty());
  s21::queue<int> new_stack_empty(std::move(my_queue_empty_));
  ASSERT_TRUE(new_stack_empty.empty());
  ASSERT_EQ(new_stack_empty.size(), 0);
}

TEST_F(QueueTest, over_operator_move) {
  s21::queue<int> new_stack_empty;
  new_stack_empty = std::move(my_queue_full_);
  ASSERT_EQ(new_stack_empty.size(), 4);
  ASSERT_FALSE(new_stack_empty.empty());
  ASSERT_EQ(new_stack_empty.front(), 10);
  s21::queue<int> temp_stack{1, 2, 3, 4, 5};
  new_stack_empty = std::move(temp_stack);
  ASSERT_EQ(new_stack_empty.size(), 5);
  ASSERT_FALSE(new_stack_empty.empty());
  ASSERT_EQ(new_stack_empty.front(), 5);
  new_stack_empty = std::move(my_queue_empty_);
  ASSERT_EQ(new_stack_empty.size(), 0);
  ASSERT_TRUE(new_stack_empty.empty());
}

TEST_F(QueueTest, over_operator_copy) {
  my_queue_full_ = my_queue_full_;
  ASSERT_FALSE(my_queue_full_.empty());
  ASSERT_EQ(my_queue_full_.front(), 10);
  ASSERT_EQ(my_queue_full_.size(), 4);
  s21::queue<int> new_stack_full{10, 20, 30};
  new_stack_full = my_queue_full_;
  ASSERT_FALSE(my_queue_full_.empty());
  ASSERT_EQ(my_queue_full_.front(), 10);
  ASSERT_EQ(my_queue_full_.size(), 4);
  new_stack_full = my_queue_empty_;
  ASSERT_FALSE(my_queue_full_.empty());
  ASSERT_EQ(my_queue_full_.front(), 10);
  ASSERT_EQ(my_queue_full_.size(), 4);
}

TEST_F(QueueTest, empty_function) {
  ASSERT_FALSE(my_queue_full_.empty());
  ASSERT_TRUE(my_queue_empty_.empty());
}

TEST_F(QueueTest, size_function) {
  ASSERT_EQ(my_queue_full_.size(), 4);
  ASSERT_EQ(my_queue_empty_.size(), 0);
}

TEST_F(QueueTest, push_functin) {
  my_queue_full_.push(32);
  my_queue_empty_.push(2);
  ASSERT_EQ(my_queue_full_.front(), 32);
  ASSERT_EQ(my_queue_empty_.front(), 2);
}

TEST_F(QueueTest, pop_function) {
  ASSERT_THROW(my_queue_empty_.pop(), std::length_error);
  my_queue_full_.pop();
  ASSERT_EQ(my_queue_full_.front(), 10);
  my_queue_full_.pop();
  ASSERT_EQ(my_queue_full_.back(), 7);
}

TEST_F(QueueTest, swap_function) {
  new_type buff_1;
  buff_1.a_char = 's';
  buff_1.a_double = 100.12;
  s21::queue<new_type> buff_new_type{buff, buff};
  s21::queue<new_type> buff_new_type_1{buff_1, buff_1, buff_1};
  buff_new_type.swap(buff_new_type_1);
  ASSERT_EQ(buff_new_type.front().a_double, 100.12);
  ASSERT_EQ(buff_new_type.front().a_char, 's');
  ASSERT_EQ(buff_new_type_1.front().a_double, 2.3);
  ASSERT_EQ(buff_new_type_1.front().a_char, 'd');
}

TEST_F(QueueTest, emplace_function) {
  my_queue_full_.emplace_back(48);
  my_queue_full_.emplace_back(68);
  my_queue_full_.emplace_back(88);
  my_queue_full_.emplace_back(108);
  my_queue_full_.pop();
  my_queue_full_.pop();
  my_queue_full_.pop();
  my_queue_full_.pop();
  for (int i = 48; i != 108; i += 20) {
    ASSERT_EQ(my_queue_full_.back(), i);
    my_queue_full_.pop();
  }
}

TEST_F(QueueTest, emplace_function_object) {
  struct Temp {
    Temp(int x) { x_tmp = x; }
    Temp() {}
    ~Temp() {}
    int x_tmp = 0;
  };
  s21::queue<Temp> tmp;
  tmp.emplace_back(2);
  ASSERT_EQ(tmp.back().x_tmp, 2);
}
