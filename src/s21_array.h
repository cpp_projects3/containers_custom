#ifndef S21_CONTAINERS_SRC_S21_ARRAY_H_
#define S21_CONTAINERS_SRC_S21_ARRAY_H_

// #include <iostream>

namespace s21 {
template <typename T, std::size_t N = 0>
class array {
 public:
  using value_type = T;
  using reference = T &;
  using const_reference = const T &;
  using iterator = T *;
  using const_iterator = const T *;
  using size_type = size_t;

  /**
   * @brief Конструктор по умолчанию
   */
  array() : size_(N){};

  /**
   * @brief initializer list constructor
   * @param items Список значений
   */
  array(std::initializer_list<value_type> const &items) {
    try {
      if (items.size() > size_) {
        throw(items.size());
      } else {
        for (size_type i = 0; i < N; ++i) {
          data_[i] = items.begin()[i];
        }
      }
    } catch (int size_list) {
      std::cout << "Initializer list's length " << (size_list)
                << "must be less than array's length" << (N) << std::endl;
    }
  };

  /**
   * @breif 	copy constructor
   * @param a Копирование с объекта класса Array
   */
  array(const array &a) noexcept { copy(a); };

  /**
   * @brief move constructor
   * @param a Объект класса array
   */
  array(array &&a) noexcept { move(a); };

  ~array() noexcept = default;

  /**
   * @brief assignment operator overload for coping object
   * @param a объект класса array (объект с которого перемещаем)
   * @return результат скопированного значения
   */
  array &operator=(const array &a) {
    if (this == &a) return *this;
    if (size_ != a.size_) {
      throw std::out_of_range("Containers capacity are not equal");
    }
    copy(a);
    return *this;
  };

  /**
   * @brief assignment operator overload for moving object
   * @param a объект класса array (объект с которого перемещаем)
   * @return результат перемещенного значения
   */
  array &operator=(array &&a) {
    if (size_ != a.size_) {
      throw std::out_of_range("Containers capacity are not equal");
    }
    move(a);
    return *this;
  };

  reference at(size_type pos) {
    if (pos >= size_) {
      throw std::out_of_range(
          "terminating with uncaught "
          "exception of type std::out_of_range: array::at");
    }
    return data_[pos];
  };

  constexpr const_reference at(size_type pos) const {
    if (pos >= size_) {
      throw std::out_of_range(
          "terminating with uncaught "
          "exception of type std::out_of_range: array::at");
    }
    return data_[pos];
  };

  reference operator[](size_type pos) { return at(pos); };

  constexpr const_reference operator[](size_type pos) const { return at(pos); };

  constexpr const_reference front() const {
    empty_ex();
    return at(0);
  };

  reference front() {
    empty_ex();
    return at(0);
  };

  constexpr const_reference back() const {
    empty_ex();
    return at(size_ - 1);
  };

  reference back() {
    empty_ex();
    return at(size_ - 1);
  };

  constexpr iterator data() {
    empty_ex();
    return data_;
  };

  constexpr const_iterator data() const {
    empty_ex();
    return data_;
  };

  constexpr iterator begin() { return data(); };

  constexpr const_iterator begin() const { return data(); };

  constexpr iterator end() { return data() + size_; };

  constexpr const_iterator end() const { return data() + size_; };

  [[nodiscard]] constexpr bool empty() const noexcept { return size_ == 0; };

  [[nodiscard]] constexpr size_type size() const noexcept { return size_; };

  [[nodiscard]] constexpr size_type max_size() const noexcept { return size_; };

  void swap(array &other) {
    for (size_type i = 0; i < size_; ++i) {
      value_type buff;
      buff = data_[i];
      data_[i] = other.data_[i];
      other.data_[i] = buff;
    }
  };

  void fill(const_reference value) {
    empty_ex();
    for (size_type i = 0; i < size_; ++i) {
      data_[i] = value;
    }
  };

 private:
  // Additional function
  void copy(const array &other) noexcept {
    for (size_type i = 0; i < size_; ++i) {
      data_[i] = other.data_[i];
    }
  };

  void move(array &other) noexcept {
    if (this != &other) {
      for (size_type i = 0; i < size_; ++i)
        data_[i] = std::move(other.data_[i]);
    }
  };

  void empty_ex() const {
    if (empty()) {
      throw std::out_of_range("Capacity of array is null!");
    }
  };

 private:
  value_type data_[N] = {};
  size_type size_ = N;
};
}  // namespace s21

#endif  // S21_CONTAINERS_SRC_S21_ARRAY_H_
