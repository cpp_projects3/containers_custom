#ifndef S21_CONTAINERS_SRC_S21_VECTOR_H_
#define S21_CONTAINERS_SRC_S21_VECTOR_H_

namespace s21 {
template <class T>
class vector {
 public:
  using value_type = T;
  using reference = T &;
  using const_reference = const T &;
  using iterator = T *;
  using const_iterator = const T *;
  using size_type = size_t;

  vector() : size_(0U), capacity_(0U), arr_(nullptr) {}

  vector(size_type n) : size_(n), capacity_(n), arr_(n ? new T[n] : nullptr) {}

  vector(std::initializer_list<value_type> const &items) {
    arr_ = new value_type[items.size()];
    int i = 0;
    for (auto it = items.begin(); it != items.end(); it++) {
      arr_[i] = *it;
      i++;
    }
    size_ = items.size();
    capacity_ = items.size();
  };

  vector(const vector &v)
      : size_(v.size_),
        capacity_(v.capacity_),
        arr_(size_ ? new T[size_] : nullptr) {
    *this = v;
  };

  vector(vector &&v) : size_(v.size_), capacity_(v.capacity_) {
    arr_ = std::move(v.arr_);
    v.arr_ = nullptr;
    v.size_ = 0;
  }

  ~vector() { delete[] arr_; }

  vector &operator=(const vector &v) {
    if (v.size_ >= capacity_) {
      reserve_more_capacity(v.size_ * 2);
    }
    size_ = v.size_;
    for (size_t i = 0; i < v.size_; i++) {
      arr_[i] = v.arr_[i];
    }
    return *this;
  }

  vector &operator=(vector &&v) {
    swap(v);
    return *this;
  }

  reference at(const size_type pos) const {
    if (pos >= size_) throw std::out_of_range("out of range");
    return arr_[pos];
  }

  reference operator[](size_type pos) const { return arr_[pos]; }

  const_reference front() const noexcept { return arr_[0]; }

  const_reference back() const noexcept { return arr_[size_ - 1]; }

  T *data() { return arr_; }

  iterator begin() noexcept { return arr_; }

  iterator end() noexcept { return (arr_ + size_); }

  bool empty() const noexcept { return size_ == 0; }

  size_type size() const noexcept { return size_; }

  size_type max_size() const noexcept {
    return ((std::numeric_limits<size_type>::max() / 2) / sizeof(value_type));
  }

  void reserve(size_type size) {
    if (size > capacity_) {
      reserve_more_capacity(size);
    }
  }

  size_type capacity() const noexcept { return capacity_; }

  void shrink_to_fit() noexcept {
    if (size_ < capacity_) {
      vector<value_type> arr_tmp(size_);
      arr_tmp = *this;
      arr_tmp.capacity_ = size_;
      swap(arr_tmp);
    }
  }

  void clear() noexcept {
    while (size_) {
      pop_back();
    }
  }

  iterator insert(iterator pos, const_reference value) noexcept {
    vector<value_type> tmp_v(size_);
    int pos_num = -1;
    int i = 0;
    for (auto it = arr_; it != arr_ + size_; it++, i++) {
      if (it == pos) {
        if (tmp_v.size_ == tmp_v.capacity_) {
          tmp_v.reserve_more_capacity(tmp_v.size_ * 2);
          tmp_v.size_++;
        }
        tmp_v.arr_[i] = value;
        pos_num = i;
        i++;
      }
      tmp_v.arr_[i] = *it;
    }
    if (pos_num >= 0) {
      *this = tmp_v;
      pos = &arr_[pos_num];
    }
    delete[] tmp_v.arr_;
    tmp_v.size_ = 0;
    tmp_v.arr_ = nullptr;
    return pos;
  }

  void erase(const iterator pos) noexcept {
    size_t i = 0;
    for (auto it = arr_; i < size_; it++, i++) {
      if (it == pos) {
        i--;
        --size_;
        continue;
      }
      arr_[i] = *it;
    }
  }

  void push_back(const_reference value) noexcept {
    if (size_ == capacity_) {
      reserve_more_capacity(size_ * 2);
    }
    arr_[size_++] = value;
  }

  void pop_back() noexcept {
    if (size_ > 0) {
      erase(arr_ + size_ - 1);
    }
  }

  void swap(vector &other) noexcept {
    std::swap(capacity_, other.capacity_);
    std::swap(size_, other.size_);
    std::swap(arr_, other.arr_);
  }

  template <class... Args>
  iterator emplace(const_iterator pos, Args &&...args) {
    iterator it_current{const_cast<value_type *>(pos)};
    for (auto value : {std::forward<Args>(args)...}) {
      it_current = insert(it_current, std::move(value));
      ++it_current;
    }
    return --it_current;
  }

  template <class... Args>
  void emplace_back(Args &&...args) {
    for (auto value : {std::forward<Args>(args)...}) {
      push_back(std::move(value));
    }
  }

 private:
  void reserve_more_capacity(size_type size) {
    if (size > capacity_) {
      value_type *buff = new value_type[size];
      for (size_type i = 0; i < size_; ++i) {
        buff[i] = std::move(arr_[i]);
      }
      delete[] arr_;
      arr_ = buff;
      capacity_ = size;
    }
  }

  size_t size_;
  size_t capacity_;
  T *arr_;
};
}  // namespace s21

#endif  // S21_CONTAINERS_SRC_S21_VECTOR_H_
